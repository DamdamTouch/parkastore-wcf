﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfProjet;

namespace WcfProjet
{
    [DataContract]

    public class listeParka
    {
        List<parka> listeparka = new List<parka>();

        parka parkaA = new parka("parka Refour", "parka de type nordique ", 135.99, "nul");
        parka parkaB = new parka("parka Mion", "parka marcel ", 105.99, "nul");
        parka parkaC = new parka("parka Ramel", "parka marron ", 115.99, "nul");
        parka parkaD = new parka("parka Rate", "parka avec ceinture noire ", 100.99, "nul");
        parka parkaE = new parka("parka Yak", "parka reversible", 125.99, "nul");
        parka parkaF = new parka("parka Member", "parka qui sent très fort", 128.99, "nul");


        [DataMember]
        public List<parka> initListe()
        {
           listeparka.Add(parkaA);
           listeparka.Add(parkaB);
           listeparka.Add(parkaC);
           listeparka.Add(parkaD);
           listeparka.Add(parkaE);
           listeparka.Add(parkaF);

           return listeparka;
        }

        [DataMember]
        public void addParka(parka _parka, List<parka> _listeparka)
        {
            _listeparka.Add(_parka);
        }

        [DataMember]
        public List<parka> returnliste()
        {
            return listeparka;
        }

    }
}