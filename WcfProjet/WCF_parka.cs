﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Security.Cryptography;
using WcfProjet;

namespace WcfProjet
{
    public class Service1 : IService1
    {
        static string url = "http://localhost:1391/";
        private static Dictionary<string, List<Parka>> Data { get; set; }
        private static List<Parka> lp { get; set; }

            
        static Service1()
        {
            Data = new Dictionary<string, List<Parka>>();
            lp = new List<Parka>();
            lp.Add(new Parka("Parka Rate", "Parka pas qui apprend à faire du karaté ! Instantanément vous devenez ceinture marron 3e dan de karaté", 100, url + "photos/parka1.jpg", 1));
            lp.Add(new Parka("Parka Ramel", "D'une jolie couleur marron, cette parka est aussi appétissante qu'un coulis de caramel", 99, url + "photos/parka2.jpg", 2));
        }
        public string GetToken(string value)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string token = GetMd5Hash(md5Hash, value);
                if (!Data.ContainsKey(token))
                {
                    Data.Add(token, new List<Parka>());
                }
                return token;
            }
        }

        public List<Parka> ListProducts(string token)
        {
            return lp;
        }

        public string AddToBasket(string token, Parka parka)
        {
            List<Parka> panier = new List<Parka>();
            if (Data.TryGetValue(token, out panier))
            {
                panier.Add(parka);
                return "Parka ajoutée au panier";
            }
            else
            {
                return "Désolé, la parka n'a pu être ajouté au panier";
            }
            
        }
        
        public List<Parka> ListBasket(string token)
        {
            List<Parka> panier = new List<Parka>();
            if (Data.TryGetValue(token, out panier))
            {
                return panier;
            }
            else
            {
                return panier;
            }
            
        }

        public string RemoveFromBasket(string token, Parka parka)
        {
            List<Parka> panier = new List<Parka>();
            if (Data.TryGetValue(token, out panier))
            {
                bool t = false;
                foreach (Parka p in panier)
                {
                    if (p.id == parka.id)
                    {
                        panier.Remove(p);
                        t = true;
                        break;
                    }
                }
                if(t == false)
                {
                    return "Erreur";
                }else
                {
                    return "Parka supprimée du panier";
                }
            }
            /*
            panier.Remove(parka);
            return "Parka supprimée du panier";
            }*/
            else
            {
                return "Désolé, la parka n'a pu être supprimée au panier";
            }
        }

        private string adminToken = "21232f297a57a5a743894a0e4a801fc3";
        public string UpdateProduct(string token, Parka parkaToUpdate)
        {
            if (token != adminToken) {
                return "Restricted access";
            }
            bool t = false;
            foreach (Parka p in lp)
            {
                if (p.id == parkaToUpdate.id)
                {
                    p.title = parkaToUpdate.title;
                    p.description = parkaToUpdate.description;
                    p.price = parkaToUpdate.price;
                    p.photo = parkaToUpdate.photo;
                    t = true;
                    break;
                }
            }
            if(t == true)
            {
                return "La modification a réussit";
            }else
            {
                return "la modification a échoué";
            }
        }

        public string AddProduct(string token, Parka parkaToAdd)
        {
            if (token != adminToken) {
                return "Restricted access";
            }
            lp.Add(parkaToAdd);
            return "Parka ajouté à la liste";
        }


       
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
