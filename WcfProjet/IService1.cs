﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfProjet
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IService1" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetToken(string token);
        [OperationContract]
        List<Parka> ListProducts(string token);
        [OperationContract]
        string AddToBasket(string token, Parka parka);
        [OperationContract]
        List<Parka> ListBasket(string token);
        [OperationContract]
        string RemoveFromBasket(string token, Parka parka);
        [OperationContract]
        string UpdateProduct(string token, Parka parkaToUpdate);
        [OperationContract]
        string AddProduct(string token, Parka parkaToAdd); 
       
    }

}
