﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace WcfProjet
{
    [DataContract]
    public class Parka
    {

        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public double price { get; set; }
        [DataMember]
        public string photo { get; set; } 
        [DataMember]
        public int id { get; set; }


        public Parka(string _title, string _description, double _price, string _photo, int _id)
        {
            title = _title;
            description = _description;
            price = _price;
            photo = _photo;
            id = _id;
        }



    }
}